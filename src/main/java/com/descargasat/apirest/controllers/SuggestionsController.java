
package com.descargasat.apirest.controllers;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.descargasat.apirest.model.City;
import com.descargasat.apirest.model.CityResponse;
import com.descargasat.apirest.model.Location;
import com.descargasat.apirest.model.QueryEngine;

@RestController
public class SuggestionsController {

	Logger LOG = LoggerFactory.getLogger(SuggestionsController.class);

	@Autowired
	private QueryEngine queryEngine;

	@RequestMapping(value = "/suggestions", method = RequestMethod.GET)
	public Map<String, Set<CityResponse>> getSuggestions(@RequestParam(name = "q") String queryName,
			@RequestParam Optional<Double> latitude, @RequestParam Optional<Double> longitude,
			HttpServletRequest request) {

		LOG.info("Consumiendo el Servicio... ");
		if (queryName == null || queryName.isEmpty()) {
			LOG.error("Parametro Recibido Vacio");
			throw new IllegalArgumentException("El parametro 'q' No puede estar Vaco");
		}

		Location defaultLocation = null;
		
		if (latitude.isPresent() && !longitude.isPresent() || !latitude.isPresent() && longitude.isPresent()) {
			throw new IllegalArgumentException("Los parametro 'latitude' y 'longitude deben enviarse juntos o omitirse ");
		} else if (latitude.isPresent() && longitude.isPresent()) {
			defaultLocation = new Location(latitude.get(), longitude.get());
		}
		
		return queryEngine.query(queryName, defaultLocation);
	}
	
	@ExceptionHandler
	public void handleIllegalArgumentException(IllegalArgumentException e, HttpServletResponse response)
			throws IOException {
		response.sendError(HttpStatus.BAD_REQUEST.value());
	}

}
