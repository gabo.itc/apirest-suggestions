// Coveo Backend Coding Challenge
// Design an API endpoint that provides auto-complete suggestions for large cities.
//
// Copyright (c) 2017 Samuel Lambert
//
// This software may be modified and distributed under the terms
// of the MIT license. See the LICENSE file for details.

package com.descargasat.apirest.model;

import org.apache.commons.lang3.text.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.descargasat.apirest.controllers.SuggestionsController;
import com.descargasat.apirest.utils.Utilerias;

/**
 * This class is an adapter to a City object with the only purpose of being
 * displayed as a JSON response.
 */
public class CityResponse implements Comparable<CityResponse> {
	private String name;
	private Double latitude;
	private Double longitude;
	private Double score;
	Logger LOG = LoggerFactory.getLogger(SuggestionsController.class);

	/**
	 * Constructs a CityResponse object
	 *
	 * @param city  city object to adapt
	 * @param score score assigned to this city
	 */
	public CityResponse(City city, Location locationQuery) {
		String state = getState(city.getCountry().toLowerCase(), city.getAdmin1().toLowerCase());
		this.name = WordUtils.capitalizeFully(city.getName(), ' ', '-') + ", " + state.toUpperCase() + ", "
				+ city.getCountry().toUpperCase();

		this.latitude = Utilerias.parseDouble(city.getLatitude());
		this.longitude = Utilerias.parseDouble(city.getLongitude());
//		this.latitude = .0;
//		this.longitude = .0;
		this.score = generateScore(locationQuery);
	}

	/**
	 * Returns formatted city name
	 *
	 * @return formatted city name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns city latitude
	 *
	 * @return city latitude
	 */
	public Double getLatitude() {
		return latitude;
	}

	/**
	 * Returns city longitude
	 *
	 * @return city longitude
	 */
	public Double getLongitude() {
		return longitude;
	}

	/**
	 * Returns city score
	 *
	 * @return city score
	 */
	public Double getScore() {
		return score;
	}

	@Override
	public int compareTo(CityResponse rhs) {
		if (name.equals(rhs.name)) {
			return 0;
		}

		//recordemos que para variables de tipo Double no se recomienda usar == para comparacion debemos obtener el valor nativo
//		if (score == rhs.score) {
		if (score.doubleValue() == rhs.score.doubleValue()) {
			return rhs.name.compareTo(name);
		}

		return rhs.score.compareTo(score);
	}

	/**
	 * Obtiene la clave del pais convirtiendolo en una clave legible para el usuario
	 * 
	 * @param country
	 * @param code
	 * @return
	 */
	public String getState(String country, String code) {
		String state = "";
		if (country.equals("us")) {
			state = code;
		} else if (country.equals("ca")) {
			if (code.equals("01"))
				state = "ab";
			else if (code.equals("02"))
				state = "bc";
			else if (code.equals("03"))
				state = "mb";
			else if (code.equals("04"))
				state = "nb";
			else if (code.equals("05"))
				state = "nl";
			else if (code.equals("07"))
				state = "ns";
			else if (code.equals("08"))
				state = "on";
			else if (code.equals("09"))
				state = "pe";
			else if (code.equals("10"))
				state = "qc";
			else if (code.equals("11"))
				state = "sk";
			else if (code.equals("12"))
				state = "yt";
			else if (code.equals("13"))
				state = "nt";
			else if (code.equals("14"))
				state = "nu";
		}
		return state;
	}

	/**
	 * Mediante la localizacion recibida y el de la ciudad genera un score
	 * 
	 * @param locationQuery
	 * @return
	 */
	public Double generateScore(Location locationQuery) {
		LOG.info("Generando Score");
		Double score = 0.0;
		if (locationQuery != null) {
			Location locationCity = new Location(this.latitude, this.longitude);
			LOG.info("locationCity: " + locationCity);
			LOG.info("locationQuery: " + locationQuery);
			LOG.info("locationMin: " + locationQuery.getLocationMinimum());
			LOG.info("locationMax: " + locationQuery.getLocationMaximum());
			Double distanciaFiltrada = Location.distance(locationCity, locationQuery);
//			Double distanciaFiltrada = Location.distance(locationQuery, locationCity);
			Double distanciaMaxima = Location.distance(locationQuery.getLocationMinimum(),
					locationQuery.getLocationMaximum());
			LOG.info("distanciaFiltrada: " + distanciaFiltrada);
			LOG.info("distanciaMaxima: " + distanciaMaxima);
			Double promedio = distanciaFiltrada / distanciaMaxima;
			LOG.info("promedio: " + promedio);
			score = 1.0 - promedio;
			LOG.info("score: " + score);
		}
		return score;
	}

}
