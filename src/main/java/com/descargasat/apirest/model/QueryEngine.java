package com.descargasat.apirest.model;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Clase que realizara el filtrado de sugerecias, generando un score por cada
 * elemento encontrado <br>
 * Creado: 25/04/2022
 * 
 * @author Gabriel Orozco Ruiz
 */
@Component
public class QueryEngine {

	/**
	 * permite el filtrado mediante contains o startWith
	 */
	private boolean filterLike = false;
	Logger LOG = LoggerFactory.getLogger(QueryEngine.class);
	private static final String CITIES_DATA_FILE = "cities_canada-usa.json";

	private List<City> cities;
//    private Map<String, City> sponsoredCities;

	/**
	 * Constructs the autocomplete trie with the data obtained in the included JSON
	 * file.
	 */
	public QueryEngine() throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

		try {
			InputStream is = classLoader.getResourceAsStream(CITIES_DATA_FILE);
			cities = Arrays.asList(mapper.readValue(is, City[].class));
		} catch (Exception e) {
			throw new IOException("Error: No fue posible parsear los datos a JSON");
		}
	}

	/**
	 * Returns sorted city suggestions according to the query string. Scores are
	 * added to each city according to its distance between itself and client
	 * location. Scores can also be set to 1.0 (maximum score possible) if the city
	 * is found to be "sponsored".
	 *
	 * @param search         cadena a buscar de las ciudades
	 * @param clientLocation location of the client
	 * @return city suggestions according to the query string
	 */
	public Map<String, Set<CityResponse>> query(String search, Location defaultLocation) {
		// Creamos e inicializamos nuestro mapa a retornar
		Map<String, Set<CityResponse>> suggestions = new HashMap<>();

		// Validamos que nuestra lista de ciudades este inicializada y no este vacia
		if (cities != null && !cities.isEmpty()) {
			/* filtramos nuestras coincidencias y lo agregamos en una nueva lista */
			List<City> list_filter = cities.stream().filter(city -> filterCity(city, search))
					.collect(Collectors.toList());
			Set<CityResponse> cityResponses = new TreeSet<>();
//
			// Adding regular suggestions
			for (City c : list_filter) {
				LOG.info("ciudad: "+c.toString());
				cityResponses.add(new CityResponse(c, defaultLocation));
			}
			LOG.info("size: "+cityResponses.size());

			suggestions.put("suggestions", cityResponses);
		}
		return suggestions;
	}

	public boolean filterCity(City city, String search) {
		boolean coincidence = false;
		if (filterLike) {
			if (city.getName().contains(search)) {
				coincidence = true;
			} else {
				coincidence = city.getAlt_name().contains(search);
			}
		} else {
			if (city.getName().startsWith(search)) {
				coincidence = true;
			} else {
				List<String> list_altern_name = Arrays.asList(city.getAlt_name().split(","));
				long count = list_altern_name.stream().filter(name -> name.startsWith(search)).count();
				coincidence = count > 0;
			}
		}
		return coincidence;
	}

}
