// Coveo Backend Coding Challenge
// Design an API endpoint that provides auto-complete suggestions for large cities.
//
// Copyright (c) 2017 Samuel Lambert
//
// This software may be modified and distributed under the terms
// of the MIT license. See the LICENSE file for details.

package com.descargasat.apirest.model;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Location {

	private static Logger LOG = LoggerFactory.getLogger(Location.class);

	private final Double latitude;
	private final Double longitude;

	private final Double latitudeMin = -90.0;
	private final Double longitudeMin = -180.0;

	private final Double latitudeMax = 90.0;
	private final Double longitudeMax = 180.0;

	/**
	 * Constructs a location object
	 *
	 * @throws IllegalArgumentException if latitude is not in the [-90.0, 90.0]
	 *                                  range or longitude is not in the [-180.0,
	 *                                  180.0] range
	 */
	public Location(Double latitude, Double longitude) {
		if (latitude >= latitudeMin && latitude <= latitudeMax && longitude >= longitudeMin
				&& longitude <= longitudeMax) {
			this.latitude = latitude;
			this.longitude = longitude;
		} else {
			throw new IllegalArgumentException("latitud y/o longitud invalida");
		}
	}

	/**
	 * Regresa la Localizacion Minima
	 *
	 * @return object Location
	 */
	public Location getLocationMinimum() {
		return new Location(this.latitudeMin, this.longitudeMin);
	}

	/**
	 * Regresa la Localizacion Maxima
	 *
	 * @return object Location
	 */
	public Location getLocationMaximum() {
		return new Location(this.latitudeMax, this.longitudeMax);
	}

	/**
	 * Returns latitude
	 *
	 * @return latitude
	 */
	public Double getLatitude() {
		return latitude;
	}

	/**
	 * Returns longitude
	 *
	 * @return longitude
	 */
	public Double getLongitude() {
		return longitude;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}

		if (other == null || getClass() != other.getClass()) {
			return false;
		}

		Location location = (Location) other;
		return Objects.equals(latitude, location.latitude) && Objects.equals(longitude, location.longitude);
	}

	/**
	 * Calcula la distancia entre 2 puntos
	 * 
	 * @param location1 localización punto 1
	 * @param location2 localización punto 2
	 * @return Devuelve la distancia entre los dos puntos
	 */
	public static Double distance(Location location1, Location location2) {
		LOG.info("Calculando Distancia");
		if (location1.equals(location2)) {
			return 0.0;
		} else {
			final Double EARTH_RADIUS = 6378.1;
//			final Double EARTH_RADIUS = 6371.0;
			Double latDistance = Math.toRadians(location2.getLatitude() - location1.getLatitude());
			Double lonDistance = Math.toRadians(location2.getLongitude() - location1.getLongitude());
			Double startLat = Math.toRadians(location1.getLatitude());
			Double endLat = Math.toRadians(location2.getLatitude());

			Double a = Math.pow(Math.sin(latDistance / 2), 2)
					+ Math.pow(Math.sin(lonDistance / 2), 2) * Math.cos(startLat) * Math.cos(endLat);
			Double c = 2 * Math.asin(Math.sqrt(a));

			return EARTH_RADIUS * c;
		}
	}

	/**
	 * Calcula la distancia entre 2 puntos
	 * 
	 * @param location1 localización punto 1
	 * @param location2 localización punto 2
	 * @param unit      unidad de calculo
	 * @return Devuelve la distancia entre los dos puntos
	 */
	public static Double distance(Location location1, Location location2, String unit) {
		LOG.info("Calculando Distancia");
		if (location1.equals(location2)) {
			return 0.0;
		} else {
			Double lonDistance = location1.getLongitude() - location2.getLongitude();
			Double dist = Math.sin(Math.toRadians(location1.getLatitude()))
					* Math.sin(Math.toRadians(location2.getLatitude()))
					+ Math.cos(Math.toRadians(location1.getLatitude()))
							* Math.cos(Math.toRadians(location2.getLatitude())) * Math.cos(Math.toRadians(lonDistance));
			dist = Math.acos(dist);
			dist = Math.toDegrees(dist);
			dist = dist * 60 * 1.1515;
			if (unit.equals("K")) {
				dist = dist * 1.609344;
			} else if (unit.equals("N")) {
				dist = dist * 0.8684;
			}
			return (dist);
		}
	}

	@Override
	public String toString() {
		return "latitude=" + latitude + ",longitude=" + longitude;
	}
}
