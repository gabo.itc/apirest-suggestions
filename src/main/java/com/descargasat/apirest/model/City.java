package com.descargasat.apirest.model;

import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

//@Entity	
public class City {
	@JsonProperty("id")
	public String id;
	@JsonProperty("name")
	public String name;
	@JsonProperty("ascii")
	public String ascii;
	@JsonProperty("alt_name")
	public String alt_name;
	@JsonProperty("latitude")
	public String latitude;
	@JsonProperty("longitude")
	public String longitude;
	@JsonProperty("feat_class")
	public String feat_class;
	@JsonProperty("feat_code")
	public String feat_code;
	@JsonProperty("country")
	public String country;
	@JsonProperty("cc2")
	public String cc2;
	@JsonProperty("admin1")
	public String admin1;
	@JsonProperty("admin2")
	public String admin2;
	@JsonProperty("admin3")
	public String admin3;
	@JsonProperty("admin4")
	public String admin4;
	@JsonProperty("population")
	public String population;
	@JsonProperty("elevation")
	public String elevation;
	@JsonProperty("dem")
	public String dem;
	@JsonProperty("tz")
	public String tz;
	@JsonProperty("modified_at")
	public String modified_at;

	/**
	 * Constructs an empty city object
	 */
	public City() {
		// Empty constructor needed by Spring..
		this.country = "us";
		this.admin1 = "oa";
	}
	

	public City(String id, String name, String ascii, String alt_name, String latitude, String longitude,
			String feat_class, String feat_code, String country, String cc2, String admin1, String admin2,
			String admin3, String admin4, String population, String elevation, String dem, String tz,
			String modified_at) {
		super();
		this.id = id;
		this.name = name;
		this.ascii = ascii;
		this.alt_name = alt_name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.feat_class = feat_class;
		this.feat_code = feat_code;
		this.country = country;
		this.cc2 = cc2;
		this.admin1 = admin1;
		this.admin2 = admin2;
		this.admin3 = admin3;
		this.admin4 = admin4;
		this.population = population;
		this.elevation = elevation;
		this.dem = dem;
		this.tz = tz;
		this.modified_at = modified_at;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAscii() {
		return ascii;
	}

	public void setAscii(String ascii) {
		this.ascii = ascii;
	}

	public String getAlt_name() {
		return alt_name;
	}

	public void setAlt_name(String alt_name) {
		this.alt_name = alt_name;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getFeat_class() {
		return feat_class;
	}

	public void setFeat_class(String feat_class) {
		this.feat_class = feat_class;
	}

	public String getFeat_code() {
		return feat_code;
	}

	public void setFeat_code(String feat_code) {
		this.feat_code = feat_code;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCc2() {
		return cc2;
	}

	public void setCc2(String cc2) {
		this.cc2 = cc2;
	}

	public String getAdmin1() {
		return admin1;
	}

	public void setAdmin1(String admin1) {
		this.admin1 = admin1;
	}

	public String getAdmin2() {
		return admin2;
	}

	public void setAdmin2(String admin2) {
		this.admin2 = admin2;
	}

	public String getAdmin3() {
		return admin3;
	}

	public void setAdmin3(String admin3) {
		this.admin3 = admin3;
	}

	public String getAdmin4() {
		return admin4;
	}

	public void setAdmin4(String admin4) {
		this.admin4 = admin4;
	}

	public String getPopulation() {
		return population;
	}

	public void setPopulation(String population) {
		this.population = population;
	}

	public String getElevation() {
		return elevation;
	}

	public void setElevation(String elevation) {
		this.elevation = elevation;
	}

	public String getDem() {
		return dem;
	}

	public void setDem(String dem) {
		this.dem = dem;
	}

	public String getTz() {
		return tz;
	}

	public void setTz(String tz) {
		this.tz = tz;
	}

	public String getModified_at() {
		return modified_at;
	}

	public void setModified_at(String modified_at) {
		this.modified_at = modified_at;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}

		if (other == null || getClass() != other.getClass()) {
			return false;
		}

		City city = (City) other;
		return Objects.equals(id, city.id) && Objects.equals(name, city.name) && Objects.equals(ascii, city.ascii)
				&& Objects.equals(alt_name, city.alt_name) && Objects.equals(latitude, city.latitude)
				&& Objects.equals(longitude, city.longitude) && Objects.equals(feat_class, city.feat_class)
				&& Objects.equals(feat_code, city.feat_code) && Objects.equals(country, city.country)
				&& Objects.equals(cc2, city.cc2) && Objects.equals(admin1, city.admin1)
				&& Objects.equals(admin2, city.admin2) && Objects.equals(admin3, city.admin3)
				&& Objects.equals(admin4, city.admin4) && Objects.equals(population, city.population)
				&& Objects.equals(elevation, city.elevation) && Objects.equals(dem, city.dem)
				&& Objects.equals(tz, city.tz) && Objects.equals(modified_at, city.modified_at);
	}

	@Override
	public String toString() {
		return "id: " + id + ",name: " + name + ",ascii: " + ascii + ",alt_name: " + alt_name + ",lat: " + latitude
				+ ",long: " + longitude + ",feat_class: " + feat_class + ",feat_code: " + feat_code + ",country: " + country
				+ ",cc2: " + cc2 + ",admin1: " + admin1 + ",admin2: " + admin2 + ",admin3: " + admin3 + ",admin4: " + admin4
				+ ",population: " + population + ",elevation: " + elevation + ",dem: " + dem + ",tz: " + tz
				+ ",modified_at: " + modified_at;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, ascii, alt_name, latitude, longitude, feat_class, feat_code, country, cc2, admin1,
				admin2, admin3, admin4, population, elevation, dem, tz, modified_at);
	}
}