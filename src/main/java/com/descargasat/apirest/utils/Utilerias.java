package com.descargasat.apirest.utils;

import org.springframework.stereotype.Component;

public class Utilerias {
	
	/**
	 * Convierte un objeto a Double, en caso de no poder parsear regresa CERO
	 * 
	 * @param valor valor a convertir
	 * @return el valor convertido
	 */
	public static Double parseDouble(Object valor) {
		Double retorno = 0.0;
		try {
			retorno = Double.parseDouble(valor.toString());
		} catch (Exception ex) {
		}
		return retorno;
	}

}
