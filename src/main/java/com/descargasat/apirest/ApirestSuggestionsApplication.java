package com.descargasat.apirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

//@EnableAutoConfiguration
//@ComponentScan(basePackages = "com.descagasat.apirest.controllers")
@SpringBootApplication
public class ApirestSuggestionsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApirestSuggestionsApplication.class, args);
	}

}
